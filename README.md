# Site ODA
Ceci est le code source du site de [ODA Energy](https://odaenergy.be). 

## Roadmap
- [x] Mettre en ligne le site
- [x] Ajouter le site sur les moteurs de recherche
- [x] Compresser les images du site pour un chargement plus rapide des pages
- [ ] Ajouter le formulaire pour le devis (en cours)
- [x] Ajouter le formulaire de contact


