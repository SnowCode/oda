var current1 = 0;
changeSlide1(current1);

// Adding timer for slideshows
setInterval(changeAllSlides, 3000);
function changeAllSlides() {
    changeSlide1(1);
    changeSlide2(1);
    changeSlide3(1);
}


function changeSlide1(n) {
    var slides = document.getElementsByClassName("slide1");
    slides[current1].style.display = "none";

    if (current1 + n < 0) {
        current1 = slides.length - 1;
    } else if (current1 + n > slides.length - 1) {
        current1 = 0;
    } else {
        current1 += n;
    }
    slides[current1].style.display = "block";
}


var current2 = 0;
changeSlide2(current2);

function changeSlide2(n) {
    var slides = document.getElementsByClassName("slide2");
    slides[current2].style.display = "none";

    if (current2 + n < 0) {
        current2 = slides.length - 1;
    } else if (current2 + n > slides.length - 1) {
        current2 = 0;
    } else {
        current2 += n;
    }
    slides[current2].style.display = "block";
}

var current3 = 0;
changeSlide3(current3);

function changeSlide3(n) {
    var slides = document.getElementsByClassName("slide3");
    slides[current3].style.display = "none";

    if (current3 + n < 0) {
        current3 = slides.length - 1;
    } else if (current3 + n > slides.length - 1) {
        current3 = 0;
    } else {
        current3 += n;
    }
    slides[current3].style.display = "block";
}
var current4 = 0;
changeSlide4(current4);

function changeSlide4(n) {
    var slides = document.getElementsByClassName("slide4");
    slides[current4].style.display = "none";

    if (current4 + n < 0) {
        current4 = slides.length - 1;
    } else if (current4 + n > slides.length - 1) {
        current4 = 0;
    } else {
        current4 += n;
    }
    slides[current4].style.display = "block";
}
