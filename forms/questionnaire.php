
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
<?php
    // replace this email with the real one
    $to = 'info@odaenergy.be';

    // Récupération des réponses du formulaire
    $type = htmlspecialchars($_POST['type']);
    $emplacement = htmlspecialchars($_POST['emplacement']);
    $alimentation = htmlspecialchars($_POST['alimentation']);
    $puissance = htmlspecialchars($_POST['puissance']);
    $marque = htmlspecialchars($_POST['marque']);
    $modele = htmlspecialchars($_POST['modele']);
    $nom = htmlspecialchars($_POST['nom']);
    $prenom = htmlspecialchars($_POST['prenom']);
    $email = htmlspecialchars($_POST['email']);
    $telephone = htmlspecialchars($_POST['telephone']);
    $demande = htmlspecialchars($_POST['demande']);

    // Type d'installation (formattage des questions avec des checkbox)
    if ($type == "murale"){
        $type = "murale";
    } else if ($type == "pied") {
        $type = "sur pied";
    } else {
        $type = "N/A";
    }

    // alimentation
    if ($alimentation == "monophase") {
        $alimentation = "Monophasée 230V";
    } else if($alimentation == "triphase1") {
        $alimentation = "Triphasée 380V 3F+N+T";
    } else if($alimentation == "triphase2") {
        $alimentation = "Triphasée 3x220 V";
    } else {
        $alimentation = "N/A";
    }

    // Mail content
    $mail_content = <<<END
    <style>
    td, th {
      border: solid black 1px;
    }
    </style>
    <p>Un utilisateur a remplis le questionaire "particuliers" dont voici les réponses :</p>
    <table>
        <thead>
            <tr><th>Question</th><th>Réponse</th></tr>
        </thead>
        <tbody>
            <tr><td>Installation</td><td>$type</td></tr>
            <tr><td>Emplacement</td><td>$emplacement</td></tr>
            <tr><td>Alimentation</td><td>$alimentation</td></tr>
            <tr><td>Puissance</td><td>$puissance A</td></tr>
            <tr><td>Marque</td><td>$marque</td></tr>
            <tr><td>Modèle</td><td>$modele</td></tr>
            <tr><td>Nom</td><td>$nom</td></tr>
            <tr><td>Prénom</td><td>$prenom</td></tr>
            <tr><td>Email</td><td>$email</td></tr>
            <tr><td>Téléphone</td><td>$telephone</td></tr>
            <tr><td>Demande spécifique</td><td>$demande</td></tr>
        </tbody>
    </table>
    <p>Si des champs sont vides ou que la réponse est N/A, cela veut probablement dire que l'utilisateur n'a pas répondu à la question.</p>
END;
    echo $mail_content;

    $subject = "Nouvelle entrée dans le formulaire particuliers";
    $from = $email;

    // Headers pour les mails html
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $headers .= "From: $from\r\n";

    if(mail($to, $subject, $mail_content, $headers)) {
        $mail_content = "<p>Voici ce qui a été envoyé à ODA:</p><hr> $mail_content";

	$headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= "From: $to\r\n";

        mail($from, "Réponses envoyées avec succès", $mail_content, $headers);
        echo "<p><b>Mail envoyé!</b></p>";
    } else {
        echo "<p></b>Erreur, le mail n'a pas pu être envoyé</b></p>";
    }
?>
    </body>
</html>
